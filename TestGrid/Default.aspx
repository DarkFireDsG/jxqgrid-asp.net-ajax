﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title id='Description'>Ejemplo de jxqGrid Anidado.</title>
    <link rel="stylesheet" href="Scripts/jqwidgets/styles/jqx.base.css" type="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1 minimum-scale=1" />
    <script type="text/javascript" src="Scripts/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="Scripts/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="Scripts/jqwidgets/jqxdata.js"></script>
    <script type="text/javascript" src="Scripts/jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="Scripts/jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="Scripts/jqwidgets/jqxmenu.js"></script>
    <script type="text/javascript" src="Scripts/jqwidgets/jqxgrid.js"></script>
    <script type="text/javascript" src="Scripts/jqwidgets/jqxgrid.selection.js"></script>
    <script type="text/javascript" src="Scripts/jqwidgets/jqxtabs.js"></script>
    <script type="text/javascript" src="Scripts/demos.js"></script>

    <script type="text/javascript">
        var InformacionAdicional = function (index, parentElement, gridElement, record) {
            var grid = $($(parentElement).children()[0]);
            $.ajax({
                type: "POST",
                url: "Servicio.asmx/GetDomicilioUsuario",
                data: "{'IDUsuario':'" + record.ID + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    //Creando fuente de información del grid anidado.
                    var nestedGridSource =
                    {
                        localdata: response.d,
                        datatype: 'json'
                    };

                    //Adaptando la información del grid anidado.
                    var nestedGridAdapter = new $.jqx.dataAdapter(nestedGridSource);

                    //Construyendo Grid con información del grid anidado.
                    if (grid != null) {
                        grid.jqxGrid({
                            source: nestedGridAdapter, width: 400, height: 110,
                            columns: [
                                { text: 'IDUsuario', datafield: 'IDUsuario', width: 100 },
                                { text: 'Calle', datafield: 'Calle', width: 100 },
                                { text: 'Numero', datafield: 'Numero', width: 100 },
                                { text: 'Estado', datafield: 'Estado', width: 100 }
                            ]
                        });
                    }
                },
                error: function (err) {
                    alert(err);
                    console.log(err);
                }
            });
        }
        $.ajax({
            type: "GET",
            url: "Servicio.asmx/GetUsuarios",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                //Creando fuente de información.
                var gridSource =
                {
                    localdata: response.d,
                    datatype: 'json'
                };

                //Adaptando la información.
                var gridDataAdapter = new $.jqx.dataAdapter(gridSource);

                //Construyendo Grid con información.
                $("#grid").jqxGrid(
                    {
                        width: 550,
                        height: 200,
                        source: gridDataAdapter,
                        rowdetails: true,
                        rowsheight: 25,
                        initrowdetails: InformacionAdicional,
                        rowdetailstemplate: { rowdetails: "<div id='grid' style='margin: 10px;'></div>", rowdetailsheight: 220, rowdetailshidden: true },
                        columns: [
                            { text: 'ID', datafield: 'ID', width: 50 },
                            { text: 'Nombre', datafield: 'Nombre', width: 100 },
                            { text: 'Edad', datafield: 'Edad', width: 100 }
                        ]
                    });
            },
            error: function (err) {
                alert(err);
                console.log(err);
            }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id='jqxWidget' style="font-size: 13px; font-family: Verdana; float: left;">
            <div id="grid"></div>
        </div>
    </form>
</body>
</html>
