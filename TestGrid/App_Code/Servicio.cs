﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Descripción breve de Servicio
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class Servicio : System.Web.Services.WebService
{

    public Servicio()
    {

        //Elimine la marca de comentario de la línea siguiente si utiliza los componentes diseñados 
        //InitializeComponent(); 
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public List<Usuario> GetUsuarios()
    {
        var listUsuario = new List<Usuario> {
               new Usuario(1,"Pedro", 27),
               new Usuario(2, "Jose", 18),
               new Usuario(3, "Mario", 22),
               new Usuario(4, "Luis", 20),
               new Usuario(5, "Pepe", 26)
        };

        return listUsuario;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<DomicilioUsuario> GetDomicilioUsuario(int IDUsuario)
    {
        var listDomicilioUsuario = new List<DomicilioUsuario> {
               new DomicilioUsuario(1,"31 Poniente", 100,"Puebla"),
               new DomicilioUsuario(1,"31.5 Poniente", 105,"Puebla, Pue."),
               new DomicilioUsuario(2,"32 Poniente", 200,"Queretaro"),
               new DomicilioUsuario(3,"33 Poniente", 300,"Chiapas"),
               new DomicilioUsuario(3,"33 Poniente", 300,"Puebla"),
               new DomicilioUsuario(3,"10 Poniente", 310,"Chiapas"),
               new DomicilioUsuario(3,"20 Poniente", 320,"Guerrero"),
               new DomicilioUsuario(4,"34 Poniente", 400,"Guerrero"),
               new DomicilioUsuario(5,"35 Poniente", 501,"CDMX"),
               new DomicilioUsuario(5,"Dirección 2", 500,"Puebla"),
               new DomicilioUsuario(5,"Dirección 3", 600,"Guerrero"),
               new DomicilioUsuario(5,"Dirección 4", 700,"Oaxaca"),
               new DomicilioUsuario(5,"Dirección 5", 800,"Queretaro"),
        };

        return listDomicilioUsuario.Where(x => x.IDUsuario == IDUsuario).ToList();
    }
}

[Serializable]
public class Usuario
{
    public int ID { get; set; }
    public string Nombre { get; set; }
    public Int16 Edad { get; set; }

    public Usuario()
    {

    }

    public Usuario(int iD, string nombre, short edad)
    {
        ID = iD;
        Nombre = nombre;
        Edad = edad;
    }
}

[Serializable]
public class DomicilioUsuario
{
    public int IDUsuario { get; set; }
    public string Calle { get; set; }
    public int Numero { get; set; }
    public string Estado { get; set; }

    public DomicilioUsuario()
    {
    }

    public DomicilioUsuario(int iDUsuario, string calle, int numero, string estado)
    {
        IDUsuario = iDUsuario;
        Calle = calle;
        Numero = numero;
        Estado = estado;
    }
}
